//
//  LoginController.swift
//  PCSWeightApp
//
//  Created by Blake Eram on 2019-01-21.
//  Copyright © 2019 BlakeEram. All rights reserved.
//

import UIKit
import UserNotifications
import Alamofire

class LoginController: UIViewController, UITextFieldDelegate {

    //outlets
    @IBOutlet weak var usernameTextInput: UITextField!
    @IBOutlet weak var hashNumberInput: UITextField!
    @IBOutlet weak var activeTextField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //sets up notification for every thursday
        let content = UNMutableNotificationContent()
        content.title = "It is Thursday"
        content.body = "Remember to put in your weight for today!"
        content.sound = UNNotificationSound.default
        
        // date to show notification
        var dateComponents = DateComponents()
        dateComponents.calendar = Calendar.current
        dateComponents.weekday = 5  // Thursday
        dateComponents.hour = 14
        dateComponents.minute = 14
        
        
        //let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: "test", content: content, trigger: trigger)
        
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.add(request) { (error) in
            if error != nil {
                // Handle any errors.
            }
        }
        
        //allows screen to move
        hashNumberInput.delegate = self
        
        //moving text input to middle of the screen
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        //function to check if any previous login information is present
        checkDefaults()
    }
    
    // moving text input to middle of screen
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    //check defaults
    func checkDefaults() {
        if UserDefaults.standard.string(forKey: "username") != "" {
            usernameTextInput.text = UserDefaults.standard.string(forKey: "username")
        }
        if UserDefaults.standard.string(forKey: "hashnumber") != "" {
            hashNumberInput.text = UserDefaults.standard.string(forKey: "hashnumber")
        } 
    }
    
    //checks to see if the user is not typing anymore and is tapping somewhere else
    @IBAction func stopTyping(_ sender: Any) {
        self.view.endEditing(true)
    }

    @IBAction func loginButtonPressed(_ sender: Any) {
       
    }
    
    //function to seque to weight input controller, checkts to see if the patient is authenticated
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if usernameTextInput.text == "" || hashNumberInput.text == "" {
            let alertController = UIAlertController(title: "Fill in data", message: "One or both inputs are not entered", preferredStyle: .alert)
            let mainMenu = UIAlertAction(title: "Okay", style: .cancel, handler: { action in return
                //what ever happens after patient submits weight
            })
            alertController.addAction(mainMenu)
            self.present(alertController, animated: true, completion: nil)
        } else {
            //Left(patientID*653,4)
            var string = "\(Int(usernameTextInput.text!)!*653)"
            string = String((string.prefix(4)))
            print(string)
            if string == hashNumberInput.text! {
                UserDefaults.standard.set(usernameTextInput.text, forKey: "username")
                UserDefaults.standard.set(hashNumberInput.text, forKey: "hashnumber")
                let recieverVC = segue.destination as! ViewController
                recieverVC.hashNumber = hashNumberInput.text!
                recieverVC.userName = usernameTextInput.text!
            }
            else{
                let alertController = UIAlertController(title: "Declined", message: "Authentication was unsuccessful", preferredStyle: .alert)
                let mainMenu = UIAlertAction(title: "Try Again", style: .cancel, handler: { action in return
                    //what ever happens after patient submits weight
                })
                alertController.addAction(mainMenu)
                self.present(alertController, animated: true, completion: nil)
            }

            
            //TODO: code to access m-Health server
            
          
        }
        
    }
    
  
    

}
