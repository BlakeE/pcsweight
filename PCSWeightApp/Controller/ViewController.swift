//
//  ViewController.swift
//  PCSWeightApp
//
//  Created by Blake Eram on 2019-01-19.
//  Copyright © 2019 BlakeEram. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    //variables
    let arrayWeights = ["LBS","KG"]
    var doubleCheck = false
    
    var hashNumber = ""
    var userName = ""
    var typeOfWeight = ""
    
    //outlets
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var weightInput: UITextField!
    @IBOutlet weak var dateAndTimePicker: UIDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //delegates
        self.picker.delegate = self
        self.picker.dataSource = self
        self.weightInput.delegate = self
        
        dateAndTimePicker.setValue(UIColor.white, forKeyPath: "textColor")
        dateAndTimePicker.setValue(false, forKeyPath: "highlightsToday")
    }
    
    //checks to see if the user stops typing and is touching something else
    @IBAction func stopTyping(_ sender: Any) {
        self.view.endEditing(true)
    }

    @IBAction func submitButtonPressed(_ sender: Any) {
        if doubleCheck == false {
            let alertController = UIAlertController(title: "Make Sure", message: "Make sure all data is correct", preferredStyle: .alert)
            let mainMenu = UIAlertAction(title: "Check", style: .cancel, handler: { action in return
                //what ever happens after patient submits weight
            })
            alertController.addAction(mainMenu)
            self.present(alertController, animated: true, completion: nil)
            doubleCheck = true
        } else {
            if weightInput.text != "" {
                let changedWeight = Double(((weightInput.text ?? nil) ?? nil)!)
                var newWeight = ""
               if typeOfWeight == "KG"{
                    newWeight = "\(Measurement(value: Double(changedWeight!), unit: UnitMass.kilograms))"
                    print("Weight in KG from KG: \(newWeight)")
                } else {
                let weightInPounds = Measurement(value: Double(changedWeight!), unit: UnitMass.pounds)
                    newWeight = "\(weightInPounds.converted(to: UnitMass.kilograms))"
                    print("Weight in KG from pounds: \(newWeight)")
                }
        
                
                let urlString = "http://pcsservices.ca/WeightWebService/api/weight/\(userName)"
                
                print("URL: \(urlString)")
                
                Alamofire.request(urlString, method: .post, parameters: ["weight": newWeight, "date": "\(dateAndTimePicker.date)"], encoding: JSONEncoding.default, headers: nil).validate().responseString {
                    response in
                    switch response.result {
                        
                    case .success:
                        print(response)
                        
                        break
                    case .failure(let error):
                        print(error)
                        print("Response String: \(response.result.value)")
                    }
                }
                
                let alertController = UIAlertController(title: "Submitted", message: "Your weight input was successful", preferredStyle: .alert)
                let mainMenu = UIAlertAction(title: "Okay", style: .cancel, handler: { action in return
                    //what ever happens after patient submits weight
                })
                alertController.addAction(mainMenu)
                self.present(alertController, animated: true, completion: nil)
                doubleCheck = false
                weightInput.text = ""
            } else {
                let alertController = UIAlertController(title: "Fill in Weight", message: "Provide Weight", preferredStyle: .alert)
                let mainMenu = UIAlertAction(title: "Okay", style: .cancel, handler: { action in return
                    //what ever happens after patient submits weight
                })
                alertController.addAction(mainMenu)
                self.present(alertController, animated: true, completion: nil)
            }
       
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
        
        let components = string.components(separatedBy: inverseSet)
        
        let filtered = components.joined(separator: "")
        
        if filtered == string {
            return true
        } else {
            if string == "." {
                let countdots = textField.text!.components(separatedBy:".").count - 1
                if countdots == 0 {
                    return true
                }else{
                    if countdots > 0 && string == "." {
                        return false
                    } else {
                        return true
                    }
                }
            }else{
                return false
            }
        }
    }
    
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return arrayWeights.count
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return arrayWeights[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch row {
        case 0:
            typeOfWeight = "LBS"
        case 1:
            typeOfWeight = "KG"
        default:
            typeOfWeight = "LBS"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view {
            label = v as! UILabel
        }
        label.font = UIFont (name: "Avenir Next", size: 20)
        label.text =  arrayWeights[row]
        label.textAlignment = .center
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let str = arrayWeights[row]
        return NSAttributedString(string: str, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
    }
    
}

